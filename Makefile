# Phony targets
.PHONY: public

# Recipes
all:
	@make --no-print-directory clean
	@make --no-print-directory stats
	@make --no-print-directory public

clean:
	@echo "Cleaning results directory..."
	@-rm -r public/
	@-rm results/*
	@printf "Done\n\n"

stats:
	-R -e "setwd('scripts'); source('generate_report.R')"

public:
	@mkdir -p public/
	@cp -r data -t public
	@cp -r scripts -t public
	@cp -r results -t public
	@mv public/results/report.html public/index.html
